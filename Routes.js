import React from 'react';
import {Router,Scene,Stack} from 'react-native-router-flux';
import Splash from './pages/Splash';
import ConfigureAvatar from './pages/ConfigureAvatar';
import monCarnet from './pages/monCarnet';
import JournalDeSante from './pages/JournalDeSante';
import Antecedants from './pages/Antecedants';
import Antecedant from './pages/Antecedant'; //this is the page to add antecedant
import conseils from './pages/conseils';
import LoginSignup from './pages/LoginSignup';
import Home from './pages/Home';
const Routes = () =>(
    <Router>
        <Stack key="root">
            <Scene key="splash" component={Splash} title="Splash"  hideNavBar={true} />
            <Scene key="loginsignup"  component={LoginSignup} title="loginsignup" hideNavBar={true} />
            <Scene key="configureAvatar" initial={true} component={ConfigureAvatar} title="Configure Avatar"    hideNavBar={true} />
            <Scene key="monCarnet"  component={monCarnet} title="Mon Carnet"      hideNavBar={true}  />
            <Scene key="home"   component={Home} title="Acceuil"      hideNavBar={true}  />
            <Scene key="JournalDeSante"  component={JournalDeSante} title="Journal De Sante"    hideNavBar={true}   />
            <Scene key="Antecedants"   component={Antecedants} title="Antecedants"    hideNavBar={true}   />
            <Scene key="Antecedant"  component={Antecedant} title="Antecedant"    hideNavBar={true}   />
            <Scene key="Conseils"  component={conseils} title="Conseils"    hideNavBar={true}   />
        </Stack>
    </Router>
);
export default Routes;

