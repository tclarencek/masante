import React, {Component} from 'react';
import {View,TouchableOpacity,Image} from 'react-native';
import {createAppContainer, createBottomTabNavigator} from 'react-navigation';
import Antecedents from './Antecedants';
import Login2 from './Login2';
import MonCarnet from './monCarnet';
import ConfigureAvatar from './ConfigureAvatar';
import BottomBar from '../components/BottomBar';
class HomeInside extends Component {
    static currentPageContext = this;
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            dialogLoading: false,
            dialogShow: false,
            dialogMessage: "Loading.."
        };


    }

    // TODO: you never installed on iOS so you have to look at this => https://reactnavigation.org/docs/en/getting-started.html#installation. requires cocoa pods
    render = () => {
        return (
            <View style={{display: "flex", height: "100%", width: "100%", backgroundColor: "white"}}>
                <HomeInsideComponent/>
                <BottomBar source={require('../assets/images/icons8_Menu_100px.png')} title="Login"
                           view={this.buttons}/>
            </View>
        )
    };

    componentDidMount() {

    }

    buttons = () => {
        return ( <View style={{
            display: "flex",
            flexDirection: "row",
            padding: 15,
            alignItems: "center",
            alignContent: "center",
            justifyContent: "space-around"

        }}>
            <TouchableOpacity onPress={() => {
                HomeInside.currentPageContext.props.navigation.navigate({routeName: 'sAA'});
                this.setState({page: 1});
            }}>
                <Image
                    source={this.state.page === 1 ? require(`../assets/images/icons/tab-icons/on/icons8_Contact_100px.png`) : require(`../assets/images/icons/tab-icons/off/icons8_Contact_100px.png`)}
                    style={{
                        height: 25,
                        width: 25
                    }}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                HomeInside.currentPageContext.props.navigation.navigate({routeName: 'sAC'});
                this.setState({page: 2});
            }}>
                <Image
                    source={this.state.page === 2 ? require(`../assets/images/icons/tab-icons/on/icons8_Adjust_100px.png`) : require(`../assets/images/icons/tab-icons/off/icons8_Adjust_100px.png`)}
                    style={{
                        height: 25,
                        width: 25
                    }}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                HomeInside.currentPageContext.props.navigation.navigate({routeName: 'sAF'});
                this.setState({page: 3});
            }}>
                <Image
                    source={this.state.page === 3 ? require(`../assets/images/icons/tab-icons/on/icons8_Task_Planning_100px.png`) : require(`../assets/images/icons/tab-icons/off/icons8_Task_Planning_100px.png`)}
                    style={{
                        height: 25,
                        width: 25
                    }}/>
            </TouchableOpacity>
        </View>);
    };

}

const bottom = createBottomTabNavigator({
    "Mon Carnet": {
        screen: MonCarnet,
    },
    "Antecedents": {
        screen: Antecedents,
    },
    "Configurer Avatar": {
        screen: ConfigureAvatar
    }
});

const HomeInsideComponent = createAppContainer(bottom);
export default HomeInside;