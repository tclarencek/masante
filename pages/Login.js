import React, {Fragment} from 'react';
import {Text, StyleSheet, View, Image, TextInput, ScrollView, TouchableHighlight} from 'react-native';
import ActionBar from '../components/ActionBar';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import LinearGradient from 'react-native-linear-gradient';
import SpecialButton from "./SpecialButton";

class Login {

  /*  operation=()=>{
        this.add_position = this.add_position.bind(this);
        this.reduce_position = this.reduce_position.bind(this);
        this.show_button = this.show_button.bind(this);
        this.state = {
            position:0
        }
    };*/

    render = () => {
        const styles = StyleSheet.create({
            cover: {
                backgroundColor: "white",
                height: "100%",
                display: "flex",
            },
            main_content: {
                padding: 20,
                height: "100%",
                display: "flex",
                flexDirection: "column",
                alignContent: 'center',
                alignItems: 'center',
                justifyContent: 'center',

            },
            image: {
                marginTop: -100,
                position: "relative"
            },
            under__scores: {
                width: "60%",
                display: "flex",
                alignContent: "center",
                justifyContent: "space-around",
                alignItems: "center",
                flexDirection: "row",
                height: 10,
                marginTop: 15,
                marginBottom: 10
            },
            under__score: {
                width: "23%",
                height: "100%",
                borderRadius: 5,
                borderColor: 'rgba(0,0,0,0.1)',
                borderWidth: 1,
            },
            back_image: {
                width: 35,
                height: 35
            }

        });
        return (

            <View style={styles.cover}>
                <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Create Account"/>
                <ScrollView style={{flexGrow: 1, width: "100%"}} contentContainerStyle={{
                    flex: 1
                }}>

                    <View style={styles.main_content}>
                        <Image source={require('../assets/images/fingerprint-identification.png')}
                               style={styles.image}/>
                        <View style={styles.under__scores}>
                            <LinearGradient style={styles.under__score} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={["#63CCE5", "#67D5B2"]}>
                                {/*<View style={styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={styles.under__score} />*/}
                            </LinearGradient>
                        </View>
                        <InputField style={{width: "100%", height: 40}}
                                    source={require('../assets/images/icons/icons8_Person_96px.png')}/>
                        <InputField style={{width: "100%", height: 40}}
                                    source={require('../assets/images/icons/icons8_Person_96px.png')}/>
                        <SelectField style={{width: "100%", height: 40}}
                                     source={require('../assets/images/icons/icons8_Gender_96px.png')}/>

                        <View style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignContent: "center",
                            alignItems: "center",
                            position: "relative",
                            top: 50,
                            padding: 10,
                            width: "100%"
                        }}>

                            <Image source={require('../assets/images/icons8_Back_100px2x.png')} style={styles.back_image}
                                   onPress={this.reduce_position}/>

                            <SpecialButton text={position === 4 ? "Continuer" : "Completer"} style={{float: "right"}}
                                           onPress={this.add_position}/>

                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    };
    add_position = () => {
        if (this.state.position < 4) {
            this.state.position++;
        }
    };

    reduce_position = () => {
        if (this.state.position > 0) {
            this.state.position--;
        }
    };

    show_button = () => {
        return false;
    };


}

export default Login;