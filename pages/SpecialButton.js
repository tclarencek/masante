import React, {Component} from 'react';
import {View, Button, StyleSheet, Text, Image, TouchableNativeFeedback, TouchableHighlight,ToastAndroid,TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default class SpecialButton extends Component {
    render() {
        return (
            <TouchableOpacity  onPress={this.props.press}>
                <TouchableOpacity  onPress={this.props.press}>
                    <View>
                        <LinearGradient colors={["#67D5B2", "#63CCE5"]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                        style={styles.button}>
                            <Button  onPress={this.props.press}
                                title={this.props.text}
                                color="transparent"

                            />
                            <Image source={require('../assets/images/icons8_Forward_100px2x.png')} style={{
                                height: 15,
                                width: 15,
                                position: "relative",
                                marginTop: 2.5,
                                marginLeft: 5
                            }} />
                        </LinearGradient>
                        {/* <LinearGradient colors={["#67D5B2","#63CCE5"]} start={{x:0,y:0}} end={{x:1,y:0}}  style={styles.under_button}>
                </LinearGradient>*/}
                    </View>
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
};
const styles = StyleSheet.create({
    button: {
        paddingLeft: 15,
        paddingRight: 20,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 25,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignContent: "center",
        alignItems: "center"
    },
    under_button: {
        borderRadius: 25,
        width: 200,
        height: 35,
        opacity: 0.14,
        position: "relative",
        top: -32
    }
});