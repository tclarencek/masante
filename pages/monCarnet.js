import React, {Fragment} from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import LinearGradient from 'react-native-linear-gradient';
import SpecialButton from "./SpecialButton";
import BottomBar from '../components/BottomBar';
import Utils from '../utils/utils';
import config from '../config/config';
import heartbeat from '../assets/images/heartbeat.svg';
import Swiper from 'react-native-swiper';
import Carousel from 'react-native-snap-carousel';

let position = 0;

class ConfigureAvatar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            dateofbirth: '',
            gender: '',
            bloodgroup: '',
            rhesus: '',
            page: 1,
            avatar: '',
            loadedAvatar: require('../assets/images/icons/avatar-placeholder.png'), //the object already loaded into memory
            age: 0,

        }
    }

    _render_item = (item, index) => {

        return (
            <View style={styles.table}>
                {/*<TouchableOpacity style={{height: 80, width: "33.33%"}}>*/}
                    {/*<View style={styles.tableItem}>*/}
                        {/*<Image source={item.item[0].image} style={styles.OptionIcon}/>*/}
                        {/*<Text style={styles.bottomText}>{item.item[0].title}</Text>*/}
                    {/*</View>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity style={{height: 80, width: "33.33%"}}>
                    <View style={styles.tableItem}>
                        <Image source={item.item[1].image} style={styles.OptionIcon}/>
                        <Text style={styles.bottomText}>{item.item[1].title}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{height: 80, width: "33.33%"}}>
                    <View style={styles.tableItem}>
                        <Image source={item.item[2].image} style={styles.OptionIcon}/>
                        <Text style={styles.bottomText}>{item.item[2].title}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{height: 80, width: "33.33%"}}>
                    <View style={styles.tableItem}>
                        <Image source={item.item[3].image} style={styles.OptionIcon}/>
                        <Text style={styles.bottomText}>{item.item[3].title}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{height: 80, width: "33.33%"}}>
                    <View style={styles.tableItem}>
                        <Image source={item.item[4].image} style={styles.OptionIcon}/>
                        <Text style={styles.bottomText}>{item.item[4].title}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{height: 80, width: "33.33%"}}>
                    <View style={styles.tableItem}>
                        <Image source={item.item[5].image} style={styles.OptionIcon}/>
                        <Text style={styles.bottomText}>{item.item[5].title}</Text>
                    </View>
                </TouchableOpacity>*/}
            </View>
        );
    };
    conduction = () => {

    };
    render = () => {
        Utils.Load(config.userDataKey, null).then((response) => {
            this.setState({...JSON.parse(response)}, () => {
                let $age = Utils.calculateDateDiff(this.state.dateofbirth).years;
                this.setState({age: $age});

                if (this.state.avatar) {
                    let $avatar = {uri: config.rootUrl + this.state.avatar};
                    this.setState({loadedAvatar: $avatar});
                }

            })
        });

        return (
            <View style={styles.cover}>
                <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Mon Carnet"
                           onButtonPress={() => {
                               this.props.navigation.openDrawer()
                           }}/>
                <View style={styles.main_content}>

                    <LinearGradient style={styles.holder} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    colors={["#63CCE5", "#67D5B2"]}>
                        <View style={styles.sub_container}>
                            <Image source={require('../assets/images/icons/avatar-placeholder.png')}
                                   style={{height: 90, width: 150, borderRadius: 40, backgroundColor: "white"}}/>
                            <View style={{paddingLeft: 20}}>
                                <Text style={{color: "white", fontSize: 20, fontWeight: "900"}}>{this.state.name}</Text>
                                <Text style={{color: "white", fontSize: 18}}>{this.state.age} ans</Text>
                                <Text style={{
                                    color: "white",
                                    fontSize: 16
                                }}>{this.state.gender === 'M' ? "Male" : "Female"}/{this.state.bloodgroup}{this.state.rhesus}</Text>
                            </View>
                        </View>
                    </LinearGradient>
                    {/*<Carousel data={[]} renderItem={this._render_item}
                              sliderWidth={Dimensions.get('window').width}
                              itemWidth={(Dimensions.get('window').width)}
                              onSnapToItem={this.indexChanged}
                    />
*/}
                </View>

            </View>
        )
    };

    indexChanged = () => {
        // console.log(this._carousel.currentIndex);
    };
}

const styles = StyleSheet.create({
    cover: {
        backgroundColor: "white",
        height: "100%",
        display: "flex",
    },
    main_content: {
        flexGrow: 1,
        position: "relative",
        top: -8,
        zIndex: -1
    },
    holder: {
        padding: 20,
        marginBottom: 20
    },
    sub_container: {
        display: "flex",
        flexDirection: "row",
        alignContent: "center",
        alignItems: "center",
    },
    table: {
        display: "flex",
        flexDirection: "row",
        paddingLeft: 10,
        paddingRight: 10,
        width: Dimensions.get('window').width,
        flexWrap:"wrap"
    },

    tableItem: {
        height: "100%",
        width: "100%",
        borderWidth: 1,
        borderColor: "rgba(0,0,0,0.05)",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        alignContent: "center",
        alignItems: "center"
    },
    bottomText: {
        color: "#63CCE4",
        fontFamily: 'Raleway',
        fontWeight: 'bold',
        marginBottom:10,
        textAlign:"center"
    },
    OptionIcon: {
        marginBottom:15,
        height: 35,
        width: 35,
        marginTop:10
    }

});
export default ConfigureAvatar;