import React,{Fragment} from 'react';
import {Text,StyleSheet,View,Image,TextInput} from 'react-native';
import ActionBar from '../components/ActionBar';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import LinearGradient from 'react-native-linear-gradient';
import SpecialButton from "./SpecialButton";
import Config from "../config/config";
const CreateAccount = () =>{
    let position = 0;
    return(
        <View style={styles.cover}>
            <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Login"/>
            <View style={styles.main_content}>
                <Image source={require('../assets/images/fingerprint-identification.png')} style={styles.image}/>
                <View style={styles.under__scores}>
                    <LinearGradient style={styles.under__score} start={{x:0,y:0}} end={{x:1,y:0}} colors = {["#63CCE5","#67D5B2"]}>
                        {/*<View style={styles.under__score} />*/}
                    </LinearGradient>
                </View>
                <InputField style={{width:"100%",height:40}} source={require('../assets/images/icons8_Person_100px_12x.png')}/>
                <InputField style={{width:"100%",height:40}} source={require('../assets/images/icons8_Person_100px_12x.png')}/>
                <SelectField style={{width:"100%",height:40}} source={require('../assets/images/icons8_Gender_100px2x.png')}/>

                <View style={{display:"flex",flexDirection:"row",justifyContent:"space-between",alignContent:"center",alignItems:"center",position:"absolute",bottom:100,padding:10,width:"100%"}}>

                        <Image source={require('../assets/images/icons8_Back_100px2x.png')}
                               style={{height: 35, width: 35}}/>

                    <SpecialButton text="Continuer" />
                </View>

            </View>
        </View>
    )
};
const styles = StyleSheet.create({
    cover:{
        backgroundColor:"white",
        height:"100%",
        display:"flex",
    }
});
export default CreateAccount;