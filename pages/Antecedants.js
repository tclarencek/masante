import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity,Text} from 'react-native';
import {createAppContainer, createBottomTabNavigator, NavigationActions} from 'react-navigation';
// import {} from 'react-navigation-tabs';
import monCarnet from './monCarnet';
import splash from './Splash';
import AA from './AntecedantsAllergiques';
import AC from './AntecedantsChirurgicaux';
import AF from './AntecedantsFamiliaux';
import AM from './AntecedantsMedicaux';
import BottomBar from '../components/BottomBar';
import FloatingActionButton from '../components/FloatingActionButton';
import {Actions} from 'react-native-router-flux';
import config from '../config/config';
import DialogInterface from "../components/DialogInterface";
import utils from '../utils/utils';


class Antecedants extends React.Component {
    static currentPageContext = null;
    static tabContexts = [null,null,null,null]; //each of the "this" of the tabs
    static antecedantData = {AA:[],AC:[],AF:[],AM:[]};
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            dialogLoading: false,
            dialogShow: false,
            dialogMessage: "Loading.."
        };
    }

    componentDidMount = ()=>{
        this.requestData();
    };

    requestData  = () =>{
        utils.HttpPostClient(config.rootUrl + config.endpoints.antecedents, {}, (response) => {
            this.setState({
                dialogLoading: false});
            Antecedants.antecedantData =  response;
            Antecedants.tabContexts[0].setState({data : response.AA});

            console.log(JSON.stringify(Antecedants.antecedantData ));

        }, () => {
            console.log("request didn't go through ");
        }, this);
    };

    render = () => {
        return (
            <View style={{display: "flex", height: "100%", width: "100%", backgroundColor: "white"}}>
                <AntecendantsComponent style={{flexGrow: 1}}/>
                <BottomBar view={this.buttons} title="Login"/>
                <FloatingActionButton style={{position: "absolute", bottom: 60, right: 10}} onPress={() => {
                    Actions.Antecedant();
                }}/>
                <DialogInterface visible={this.state.dialogShow} oneButton={true} onCancel={() => {
                }} negativeButton="" loading={this.state.dialogLoading} positiveButton="OK" title="Information"
                                 content={<Text style={{marginTop: 10}}>{this.state.dialogMessage}</Text>}
                                 onAccept={() => {
                                     this.setState({dialogShow: false});
                                 }}/>
            </View>
        )
    };
    buttons = () => {
        return ( <View style={{
            display: "flex",
            flexDirection: "row",
            padding: 15,
            alignItems: "center",
            alignContent: "center",
            justifyContent: "space-around"

        }}>
            <TouchableOpacity onPress={() => {
                Antecedants.currentPageContext.props.navigation.navigate({routeName: 'sAA'});
                this.setState({page: 1});
            }}>
                <Image
                    source={this.state.page === 1 ? require(`../assets/images/icons/tab-icons/on/icons8_Edit_Property_100px.png`) : require(`../assets/images/icons/tab-icons/off/icons8_Edit_Property_100px.png`)}
                    style={{
                        height: 25,
                        width: 25
                    }}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                Antecedants.currentPageContext.props.navigation.navigate({routeName: 'sAC'});
                this.setState({page: 2});
            }}>
                <Image
                    source={this.state.page === 2 ? require(`../assets/images/icons/tab-icons/on/icons8_Reproduction_100px.png`) : require(`../assets/images/icons/tab-icons/off/icons8_Reproduction_100px.png`)}
                    style={{
                        height: 25,
                        width: 25
                    }}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                Antecedants.currentPageContext.props.navigation.navigate({routeName: 'sAF'});
                this.setState({page: 3});
            }}>
                <Image
                    source={this.state.page === 3 ? require(`../assets/images/icons/tab-icons/on/icons8_Sneeze_100px.png`) : require(`../assets/images/icons/tab-icons/off/icons8_Sneeze_100px.png`)}
                    style={{
                        height: 25,
                        width: 25
                    }}/>
            </TouchableOpacity>


            <TouchableOpacity onPress={() => {
                Antecedants.currentPageContext.props.navigation.navigate({routeName: 'sAM'});
                this.setState({page: 4});
            }}>
                <Image
                    source={this.state.page === 4 ? require(`../assets/images/icons/tab-icons/on/icons8_Surgical_100px.png`) : require(`../assets/images/icons/tab-icons/off/icons8_Surgical_100px.png`)}
                    style={{
                        height: 25,
                        width: 25
                    }}/>
            </TouchableOpacity>


        </View>);
    }
}


const styles = StyleSheet.create({
    action_bar: {
        display: "flex",
        flexDirection: "row",
        padding: 15,
        alignItems: "center",
        alignContent: "center",
        justifyContent: "space-around"

    }, text: {
        fontSize: 20,
        marginLeft: 20,
        fontFamily: 'Raleway',
        color: "rgba(0,0,0,0.8)"
    },
    image: {
        height: 25,
        width: 25
    }
});
const bottomTabNavigation = createBottomTabNavigator({
    sAA: {
        screen: AA,
        navigationOptions: {
            tabBarVisible: false
        }
    },
    sAC: {
        screen: AC,
        navigationOptions: {
            tabBarVisible: false
        }
    },
    sAF: {
        screen: AF,
        navigationOptions: {
            tabBarVisible: false
        }
    },
    sAM: {
        screen: AM,
        navigationOptions: {
            tabBarVisible: false
        }
    }
});

const AntecendantsComponent = createAppContainer(bottomTabNavigation);
export default Antecedants;