import React,{Fragment} from 'react';
import {Text,StyleSheet,View,Image,TextInput,ScrollView} from 'react-native';
import ActionBar from '../components/ActionBar';
import BottomBar from '../components/BottomBar';
import ListItemTwo from '../components/ListItemTwo';
import Antecedants from "./Antecedants";
import config from '../config/config';
import Actions from 'react-native-router-flux';

let position=0;
class AntecedantsMedicaux extends  React.Component {
    constructor(props){
        super(props);
        Antecedants.currentPageContext =this;
        Antecedants.tabContexts[3]=this;
        this.state={data:Antecedants.antecedantData.AM}
    }
    render = () => {
        let counter=0;
        return (
            <View style={styles.cover}>
                <ActionBar source={require('../assets/images/icons/icons8_Back_104px.png')} title="Antécédents Médicaux"  onButtonPress={()=>{Actions.pop()}}/>
                <ScrollView style={styles.main_content}>
                    <View style={{height: "100%", width: "100%"}} contentContainerStyle={{
                        flex: 1
                    }}>
                        {!this.state.data.length &&
                        <View style={{padding:10,display:"flex",flexDirection:"column",alignItems:"center",alignContent:"center",justifyContent:"center"}}>
                            <Image source={require('../assets/images/icons/icons8_Empty_Trash_100px.png')} style={{height:100,width:100}}/>
                            <Text style={{marginTop:10,textAlign:"center"}}>
                                {config.nothing_to_show}
                            </Text>
                        </View>
                        }

                        {this.state.data.map(am => <ListItemTwo {...am} number={++counter} />)}
                    </View>
                </ScrollView>

            </View>
        )
    };
}

const styles = StyleSheet.create({
    cover:{
        backgroundColor:"white",
        height:"100%",
        display:"flex",
    },
    main_content:{
        flexGrow:1,
        position:"relative",
        zIndex:1
       /* top:-10*/
    },
    sub_container: {
        display:"flex",
        flexDirection:"row",
        alignContent:"center",
        alignItems:"center",
    },
    table:{
        display:"flex"
    }

});
export default AntecedantsMedicaux;