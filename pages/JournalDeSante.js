import React,{Fragment} from 'react';
import {Text,StyleSheet,View,Image,TextInput,ScrollView} from 'react-native';
import ActionBar from '../components/ActionBar';
import BottomBar from '../components/BottomBar';
import ListItemTwo from '../components/ListItemTwo';

let position=0;
const JournalDeSante = () =>{

    return(
        <View style={styles.cover}>
            <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Journal De Sant&eacute;"/>
            <ScrollView style={styles.main_content}>
                   <View style={{height:"100%",width:"100%"}} contentContainerStyle={{
                       flex: 1
                   }}>
                       <ListItemTwo/>
                       <ListItemTwo/>
                       <ListItemTwo/>
                       <ListItemTwo/>
                       <ListItemTwo/>
                       <ListItemTwo/>
                       <ListItemTwo/>
                       <ListItemTwo/>
                       <ListItemTwo/>
                   </View>
            </ScrollView>
            <BottomBar source={require('../assets/images/icons8_Menu_100px.png')} title="Login"  />
        </View>
    )
};


const styles = StyleSheet.create({
    cover:{
        backgroundColor:"white",
        height:"100%",
        display:"flex",
    },
    main_content:{
        flexGrow:1,
        position:"relative",
        zIndex:1
       /* top:-10*/
    },
    sub_container: {
        display:"flex",
        flexDirection:"row",
        alignContent:"center",
        alignItems:"center",
    },
    table:{
        display:"flex"
    }

});
export default JournalDeSante;