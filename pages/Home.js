import React, {Component} from 'react';
import {View,TouchableOpacity,Image} from 'react-native';
import {createAppContainer, createDrawerNavigator} from 'react-navigation';
import Antecedents from './Antecedants';
import Login2 from './Login2';
import monCarnet from './monCarnet';
import ConfigureAvatar from './ConfigureAvatar';
import BottomBar from '../components/BottomBar';
class Home extends Component {
    static currentPageContext = this;
    constructor(props) {
        super(props);
    }
    render = () => {
        return (
                <HomeComponent/>
        )
    };

}

const MyDrawerNavigator = createDrawerNavigator({
    "Mon Carnet": {
        screen: monCarnet,
    },
    "Antecedents": {
        screen: Antecedents,
    },
    "Configurer Avatar": {
        screen: ConfigureAvatar
    }
});

const HomeComponent = createAppContainer(MyDrawerNavigator);
export default Home;