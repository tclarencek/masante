import React, {Fragment} from 'react';
import {Text, StyleSheet, View, Image, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import ActionBar from '../components/ActionBar';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import DateField from '../components/DateField';
import LinearGradient from 'react-native-linear-gradient';
import SpecialButton from "./SpecialButton";
import config from '../config/config';
import {Actions} from 'react-native-router-flux';
import DialogInterface from "../components/DialogInterface";
import utils from '../utils/utils';
import Antecedants from './Antecedants';

class Antecedant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nom: '',
            type: config.typeDAntecedants[0].value,
            date: new Date(),
            place: '',
            dialogLoading: false,
            dialogShow: false,
            dialogMessage: "Loading.."
        };
        this.validateForm = this.validateForm.bind(this);
    }

    render = () => {

        return (

            <View style={styles.cover}>
                <ActionBar source={require('../assets/images/icons/icons8_Back_104px.png')} title="Antécédent"
                           onButtonPress={() => {
                               Actions.pop();
                           }}/>
                <ScrollView style={{flexGrow: 1, width: "100%"}} contentContainerStyle={{
                    flex: 1
                }}>

                    <View style={styles.main_content}>
                        <Image source={require('../assets/images/icons/icons8_Order_History_100px.png')}
                               style={styles.image}/>
                        <View style={styles.under__scores}>
                            <LinearGradient style={styles.under__score} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={["#63CCE5", "#67D5B2"]}>
                                {/*<View style={styles.under__score} />*/}
                            </LinearGradient>
                        </View>
                        <Text style={{
                            fontFamily: "Raleway",
                            fontSize: 17,
                            color: "rgba(0,0,0,0.41)",
                            marginTop: 3,
                            marginBottom: 3
                        }}>
                            Entrer vos Antécédents
                        </Text>
                        <InputField style={{width: "100%", height: 40}}
                                    source={require('../assets/images/icons/icons8_Select_All_100px.png')}
                                    placeholder="Nom" value={this.state.nom} change={nom => {
                            this.setState({nom})
                        }}/>
                        <DateField style={{width: "100%", height: 40}}
                                   source={require('../assets/images/icons/icons8_Calendar_96px.png')}
                                   placeholder="Date" date={this.state.date}
                                   change={(date) => {
                                       this.setState({date})
                                   }}/>
                        <SelectField elements={config.typeDAntecedants} style={{width: "100%", height: 40}}
                                     source={require('../assets/images/icons/icons8_Select_All_100px.png')}
                                     placeholder="Type d'antécédent" value={this.state.type} change={type => {
                            this.setState({type})
                        }}/>
                        <InputField style={{width: "100%", height: 40}}
                                    source={require('../assets/images/icons/icons8_Place_Marker_100px.png')}
                                    placeholder="Lieu" value={this.state.place} change={place => {
                            this.setState({place})
                        }}/>
                        <View style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignContent: "center",
                            alignItems: "center",
                            position: "relative",
                            top: 50,
                            padding: 10,
                            width: "100%"
                        }}>

                            <TouchableOpacity onPress={() => {
                                Actions.pop();
                            }}>
                                <Image source={require('../assets/images/icons/icons8_Xbox_X_100px.png')}
                                       style={styles.back_image}
                                />
                            </TouchableOpacity>

                            <SpecialButton text="Sauvegarder" style={{float: "right"}} press={this.validateForm}
                            />

                        </View>

                    </View>
                </ScrollView>
                <DialogInterface visible={this.state.dialogShow} oneButton={true} onCancel={() => {
                }} negativeButton="" loading={this.state.dialogLoading} positiveButton="OK" title="Information"
                                 content={<Text style={{marginTop: 10}}>{this.state.dialogMessage}</Text>}
                                 onAccept={() => {
                                     this.setState({dialogShow: false});
                                 }}/>
            </View>
        )
    };

    validateForm() {

        let _state = this.state;
        // ToastAndroid.showWithGravity(_state,ToastAndroid.SHORT,ToastAndroid.CENTER);
        console.log(_state);
        for (let _key in _state) {
            let _value = _state[_key];
            console.log(_value);
            if (_key.includes("dialog")) {
                console.log("ignoring " + _key);
                break;
            }
            if (_value) {
                console.log("ok")
            } else {
                console.log(_key + " not ok");
                this.setState({
                    dialogLoading: false,
                    dialogShow: true,
                    dialogMessage: "Veuillez remplir tous les champs"
                });
                return;
            }
        }

        // in case everything looks good
        //
        utils.HttpPostClient(config.rootUrl + config.endpoints.antecedentCreate, _state, (response) => {
            this.setState({
                dialogLoading: false
            });
            console.log("adding: " + JSON.stringify(response.data) + " before poping from stack");
            let ctx = null;
            if (this.state.type === 'AA') {
                ctx = Antecedants.tabContexts[0];
            } else if (this.state.type === 'AC') {
                ctx = Antecedants.tabContexts[1];
            } else if (this.state.type === 'AF') {
                ctx = Antecedants.tabContexts[2];
            } else if (this.state.type === 'AM') {
                ctx = Antecedants.tabContexts[3];
            } else {
                console.log('neither AA nor AC nor AF nor AM');
            }
            if (ctx) {
                let _data = ctx.state.data;
                _data.push(response.data);
                ctx.setState({data: _data});
            }
            //add it to the list before popping..
            Actions.pop();

        }, () => {
            console.log("request didn't go through ");
        }, this);

    }
}

const styles = StyleSheet.create({
    cover: {
        backgroundColor: "white",
        height: "100%",
        display: "flex",
    },
    main_content: {
        padding: 20,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',

    },
    image: {
        marginTop: -100,
        position: "relative",
        height: 90,
        width: 90
    },
    under__scores: {
        width: "60%",
        display: "flex",
        alignContent: "center",
        justifyContent: "space-around",
        alignItems: "center",
        flexDirection: "row",
        height: 10,
        marginTop: 15,
        marginBottom: 10
    },
    under__score: {
        width: "23%",
        height: "100%",
        borderRadius: 5,
        borderColor: 'rgba(0,0,0,0.1)',
        borderWidth: 1,
    },
    back_image: {
        width: 35,
        height: 35
    }

});


export default Antecedant;