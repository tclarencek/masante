import React, {Fragment} from 'react';
import {Text, StyleSheet, View, Image, ScrollView, TouchableHighlight} from 'react-native';
import ActionBar from '../components/ActionBar';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import LinearGradient from 'react-native-linear-gradient';
import SpecialButton from "./SpecialButton";
import PasswordField from "../components/PasswordField";
import DialogInterface from "../components/DialogInterface";
import {Actions} from 'react-native-router-flux';
import utils from '../utils/utils';
import config from '../config/config';
class Login2 extends React.Component {
    constructor(props){
        super(props);
        this.state = {email:'',password:'',dialogLoading:false,
            dialogShow:false,
            dialogMessage:"Loading.."};
        this.validateForm = this.validateForm.bind(this);
    }
    static icon_style = StyleSheet.create({
        icon: {
            width: 24,
            height: 24,
        }
    });
    static navigationOptions = {
        title: 'Connexion',
        drawerLabel: 'Connexion',
        drawerIcon: ({tintColor}) => (
            <Image
                source={require('../assets/images/icons/login.png')}
                style={[Login2.icon_style.icon, {tintColor: tintColor}]}
            />
        )
    };
    render = () => {
        return (

            <View style={this.styles.cover}>
                <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Connexion"  onButtonPress={()=>{this.props.navigation.openDrawer()}}/>
                <ScrollView style={{flexGrow: 1, width: "100%"}} contentContainerStyle={{
                    flex: 1
                }}>

                    <View style={this.styles.main_content}>
                        <Image source={require('../assets/images/icons/fingerprint.png')}
                               style={this.styles.image}/>
                        <View style={this.styles.under__scores}>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={["#63CCE5", "#67D5B2"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                        </View>
                        <Text style={{
                            fontFamily: "Raleway",
                            fontSize: 17,
                            color: "rgba(0,0,0,0.41)",
                            marginTop: 3,
                            marginBottom: 3
                        }}>
                            Entrer vos informations
                        </Text>
                        <InputField style={{width: "100%", height: 40}}
                                    source={require('../assets/images/icons/icons8_Send_Email_100px.png')} value={this.state.email} placeholder="Adresse mail" change={(email)=>{this.setState({email})}}/>
                        <PasswordField style={{width: "100%", height: 40}}
                                       source={require('../assets/images/icons/icons8_Place_Marker_96px.png')} value={this.state.password} placeholder="***" change={(password)=>{this.setState({password})}}/>
                        <View style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignContent: "center",
                            alignItems: "center",
                            position: "relative",
                            top: 50,
                            padding: 10,
                            width: "100%"
                        }}>

                            <Image source={require('../assets/images/icons/icons8_Xbox_X_100px.png')}
                                   style={this.styles.back_image}
                            />

                            <SpecialButton text="Valider" style={{float: "right"}} press={this.validateForm}
                            />
                            <DialogInterface visible={this.state.dialogShow} oneButton={true} onCancel={()=>{}} negativeButton="" loading={this.state.dialogLoading} positiveButton="OK" title="Information" content={<Text style={{marginTop:10}}>{this.state.dialogMessage}</Text>} onAccept={()=>{
                                this.setState({dialogShow:false});
                            }}/>

                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    };
    validateForm(){

        let _state =  this.state;
        // ToastAndroid.showWithGravity(_state,ToastAndroid.SHORT,ToastAndroid.CENTER);
        console.log(_state);
        for(let _key in _state){
            let _value =  _state[_key];
            console.log(_value);
            if(_key.includes("dialog")){
                console.log("ignoring " + _key);
                break;
            }
            if(_value){
                console.log("ok")
            }else{
                console.log(_key + " not ok");
                this.setState({dialogLoading:false,dialogShow:true,dialogMessage:"Veuillez remplir tous les champs"});
                return;
            }
        }

        // in case everything looks good
        //
        utils.HttpPostClient(config.rootUrl + config.endpoints.login, _state, (response) => {
            this.setState({ dialogLoading: true});
            console.log("received "+JSON.stringify(response.data));
            utils.Store(config.userDataKey, JSON.stringify(response.data)).then(() => {
                this.setState({
                    dialogLoading: false,
                    // dialogShow: true,
                    // dialogMessage: config.successFulSignUpMessage,
                    // onAccept: () => {
                    //     this.setState({dialogShow: false});
                    //     Actions.monCarnet();
                    // }
                });
                Actions.monCarnet();
            });
        }, () => {
            console.log("request didn't go through ");
        }, this);

    }
    styles = StyleSheet.create({
        cover: {
            backgroundColor: "white",
            height: "100%",
            display: "flex",
        },
        main_content: {
            padding: 20,
            height: "100%",
            display: "flex",
            flexDirection: "column",
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',

        },
        image: {
            marginTop: -100,
            position: "relative",
            height: 90,
            width: 90
        },
        under__scores: {
            width: "60%",
            display: "flex",
            alignContent: "center",
            justifyContent: "space-around",
            alignItems: "center",
            flexDirection: "row",
            height: 10,
            marginTop: 15,
            marginBottom: 10
        },
        under__score: {
            width: "23%",
            height: "100%",
            borderRadius: 5,
            borderColor: 'rgba(0,0,0,0.1)',
            borderWidth: 1,
        },
        back_image: {
            width: 0,
            height: 0
        },


    });
}

export default Login2;