import React, {Component} from 'react';
import {View} from 'react-native';
import {createAppContainer,createDrawerNavigator,createSwitchNavigator,createStackNavigator} from 'react-navigation';
import CreateAccount2 from './CreateAccount2';
import Login2 from './Login2';

class LoginSignup extends Component{
    // TODO: you never installed on iOS so you have to look at this => https://reactnavigation.org/docs/en/getting-started.html#installation. requires cocoa pods
    render = () => {
        return (
            <LoginSignupComponent/>
        )
    };
    componentDidMount(){

    }

}
//
// const LoginStackNavigator = createStackNavigator(
//     {
//         LoginNavigator: Login2
//     },
//     {
//         defaultNavigationOptions: ({ navigation }) => {
//             return {
//                 headerLeft: (
//                     <Icon
//                         style={{ paddingLeft: 10 }}
//                         onPress={() => navigation.openDrawer()}
//                         name="md-menu"
//                         size={30}
//                     />
//                 )
//             };
//         }
//     }
// );
//
// const SignupStackNavigator = createStackNavigator(
//     {
//         WelcomeNavigator: CreateAccount2
//     },
//     {
//         defaultNavigationOptions: ({ navigation }) => {
//             return {
//                 headerLeft: (
//                     <Icon
//                         style={{ paddingLeft: 10 }}
//                         onPress={() => navigation.openDrawer()}
//                         name="md-menu"
//                         size={30}
//                     />
//                 )
//             };
//         }
//     }
// );
// const AppDrawerNavigator = createDrawerNavigator({
//     Login: {
//         screen: LoginStackNavigator
//     },
//     Signup: {
//         screen: SignupStackNavigator
//     },
// });
//
// const AppSwitchNavigator = createSwitchNavigator({
//     Login: { screen: AppDrawerNavigator },
//     Signup: { screen: CreateAccount2 },
//
// });
//

const MyDrawerNavigator = createDrawerNavigator({
    Signup: {
        screen: CreateAccount2,
    },
    Login: {
        screen: Login2,
    },
});




const LoginSignupComponent = createAppContainer(MyDrawerNavigator);
export default LoginSignup;