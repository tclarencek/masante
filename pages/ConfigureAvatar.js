import React, {Fragment} from 'react';
import {Text, StyleSheet, View, Image, TextInput, TouchableOpacity} from 'react-native';
import ActionBar from '../components/ActionBar';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import LinearGradient from 'react-native-linear-gradient';
import SpecialButton from "./SpecialButton";
import DialogInterface from '../components/DialogInterface';
import Utils from '../utils/utils';
let position = 0;

class ConfigureAvatar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fileChoosen: '',
            dialogLoading: false,
            dialogShow: false,
            dialogMessage: "Loading.."
        }
    }

    render = () => {
        return (
            <View style={styles.cover}>
                <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Configure Avatar"
                           onButtonPress={() => {
                               this.props.navigation.openDrawer()
                           }}/>
                <View style={styles.main_content}>
                    <TouchableOpacity onPress={()=>{Utils.chooseFile("Selectionnez votre avatar",this)}}>
                        <LinearGradient style={styles.holder} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                        colors={["#63CCE5", "#67D5B2"]}>
                            <Image source={this.state.fileChoosen?{uri:this.state.fileChoosen}:require('../assets/images/icons8_Compact_Camera_100px2x.png')}/>
                        </LinearGradient>
                    </TouchableOpacity>

                    <SpecialButton text="Changer"/>
                </View>
                <DialogInterface visible={this.state.dialogShow} oneButton={true} onCancel={() => {
                }} negativeButton="" loading={this.state.dialogLoading} positiveButton="OK" title="Information"
                                 content={<Text style={{marginTop: 10}}>{this.state.dialogMessage}</Text>}
                                 onAccept={() => {
                                     this.setState({dialogShow: false});
                                 }}/>
            </View>
        )
    };
}

const styles = StyleSheet.create({
    cover: {
        backgroundColor: "white",
        height: "100%",
        display: "flex",
    },
    main_content: {
        flexGrow: 1,
        padding: 20,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    holder: {
        padding: 30,
        borderRadius: 200,
        marginBottom: 30
    }

});
export default ConfigureAvatar;