
import React, {Fragment} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image
} from 'react-native';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import utils from '../utils/utils';
class Splash extends React.Component {
    styles = StyleSheet.create({
        scrollView: {
            backgroundColor: Colors.lighter,
        },
        logo_text: {
            color: "white",
            fontSize: 30,
            marginTop: 10,
            fontFamily: 'Raleway'
        },
        bottom_text: {
            position: 'absolute',
            color: 'white',
            fontFamily: 'Raleway',
            bottom: 20
        },
        body: {
            // backgroundColor: "linear-gradient(#67D5B2,#63CCE5)",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center'
        },

    });
    render = () =>{
        const loadNextPage = () =>{
            // Action.login();
        };
        return (
            <Fragment>
                <StatusBar barStyle="dark-content"/>
                <SafeAreaView>

                    <LinearGradient  colors = {["#63CCE5","#67D5B2"]} style={this.styles.body}>
                        <Image source={require('../assets/images/heartbeat.png')}  style={{height:125,width:145}}/>

                        <Text style={this.styles.logo_text}>Ma Sant&eacute;</Text>
                        <Text style={this.styles.bottom_text}> &copy; All rights reserved health solutions</Text>
                    </LinearGradient>

                </SafeAreaView>
            </Fragment>
        )
    };
    componentDidMount(){
        setTimeout(function(){
            utils.userLoggedIn().then((response)=> {
                if (response) {
                    Actions.home();
                } else {
                    Actions.loginsignup();
                }
            });
        },3000);
    }
}
export default Splash;