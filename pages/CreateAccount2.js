import React, {Component} from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableHighlight,
    ToastAndroid,
    Button,
    TouchableWithoutFeedback
} from 'react-native';
import ActionBar from '../components/ActionBar';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import LinearGradient from 'react-native-linear-gradient';
import SpecialButton from "./SpecialButton";
import DateField from "../components/DateField";
import config from '../config/config';
import PasswordField from '../components/PasswordField';
import DialogInterface from '../components/DialogInterface';
import Preloader from '../components/Preloader';
import utils from '../utils/utils';
import {Actions} from 'react-native-router-flux';

class CreateAccount2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: 0,
            name: "",
            email: "",
            password: "",
            gender: config.genders[0].value,
            dateofbirth: new Date(),
            bloodgroup: config.bloodgroups[0].value,
            rhesus: config.rhesus[0].value,
            nomassurance: "",
            numeroassurance: "",
            complementaryassurance: "",
            dialogLoading: false,
            dialogShow: false,
            dialogMessage: "Loading.."
        };
        this.validateForm = this.validateForm.bind(this);
    }

    static icon_style = StyleSheet.create({
        icon: {
            width: 24,
            height: 24,
        }
    });
    static navigationOptions = {
        title: 'Inscription',
        drawerLabel: 'Inscription',
        drawerIcon: ({tintColor}) => (
            <Image
                source={require('../assets/images/icons/signup.png')}
                style={[CreateAccount2.icon_style.icon, {tintColor: tintColor}]}
            />
        )
    };
    static dialogData = {};

    render = () => {
        console.log(CreateAccount2.dialogData.show);
        return (

            <View style={this.styles.cover}>
                {/*<Preloader/>*/}
                <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Inscription"
                           onButtonPress={() => {
                               this.props.navigation.openDrawer();
                           }}/>
                <ScrollView style={{flexGrow: 1, width: "100%"}} contentContainerStyle={{
                    flex: 1
                }}>

                    <View style={this.styles.main_content}>
                        {this.state.position === 0 && <View style={{marginTop: 50}}/>}
                        <Image source={require('../assets/images/icons/mobile-app.png')}
                               style={this.styles.image}/>
                        {this.state.position === 0 && <View style={this.styles.under__scores}>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={["#63CCE5", "#67D5B2"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                        </View>
                        }
                        {this.state.position === 1 && <View style={this.styles.under__scores}>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={["#63CCE5", "#67D5B2"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>

                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                        </View>
                        }

                        {this.state.position === 2 && <View style={this.styles.under__scores}>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{
                                x: 1,
                                y: 0
                            }} colors={["rgba(0,0,0,0)", "rgba(0,0,0,0)"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                            <LinearGradient style={this.styles.under__score} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                            colors={["#63CCE5", "#67D5B2"]}>
                                {/*<View style={this.styles.under__score} />*/}
                            </LinearGradient>
                        </View>
                        }

                        <Text style={{
                            fontFamily: "Raleway",
                            fontSize: 17,
                            color: "rgba(0,0,0,0.41)",
                            marginTop: 3,
                            marginBottom: 3
                        }}>
                            Entrer vos informations
                        </Text>
                        {this.state.position === 0 && <View styles="">
                            <InputField style={{width: "100%", height: 40}}
                                        source={require('../assets/images/icons/icons8_Person_96px.png')}
                                        placeholder="Nom & Prenom" value={this.state.name}
                                        change={(text) => this.setState({name: text})}/>
                            <InputField style={{width: "100%", height: 40}}
                                        source={require('../assets/images/icons/icons8_Send_Email_100px.png')}
                                        placeholder="Adresse Mail" value={this.state.email} change={email => {
                                this.setState({email})
                            }}/>
                            <SelectField elements={config.genders} style={{width: "100%", height: 40}}
                                         source={require('../assets/images/icons/icons8_Gender_96px.png')}
                                         placeholder="Sexe" value={this.state.gender} change={gender => {
                                this.setState({gender})
                            }}/>
                            <PasswordField style={{width: "100%", height: 40}}
                                           source={require('../assets/images/icons/icons8_Secure_100px.png')}
                                           placeholder="****" value={this.state.password} change={password => {
                                this.setState({password})
                            }}/>
                        </View>}

                        {this.state.position === 1 && <View styles="">
                            <DateField style={{width: "100%", height: 40}}
                                       source={require('../assets/images/icons/icons8_Calendar_96px.png')}
                                       placeholder="Date de naissance" date={this.state.dateofbirth}
                                       change={(dateofbirth) => {
                                           this.setState({dateofbirth})
                                       }}/>
                            <SelectField elements={config.bloodgroups} style={{width: "100%", height: 40}}
                                         source={require('../assets/images/icons/icons8_Syringe_100px.png')}
                                         placeholder="Groupe sanguin" value={this.state.bloodgroup}
                                         change={bloodgroup => {
                                             this.setState({bloodgroup})
                                         }}/>
                            <SelectField elements={config.rhesus} style={{width: "100%", height: 40}}
                                         source={require('../assets/images/icons/icons8_Rh+_96px.png')}
                                         placeholder="Rhesus" value={this.state.rhesus} change={rhesus => {
                                this.setState({rhesus})
                            }}/>
                        </View>}

                        {this.state.position === 2 && <View styles="">
                            <InputField style={{width: "100%", height: 40}}
                                        source={require('../assets/images/icons/icons8_Lease_96px.png')}
                                        placeholder="Nom assurance maladie " value={this.state.nomassurance}
                                        change={(nomassurance) => {
                                            this.setState({nomassurance})
                                        }}/>
                            <InputField style={{width: "100%", height: 40}}
                                        source={require('../assets/images/icons/icons8_Treatment_100px.png')}
                                        placeholder="Numéro assurance " value={this.state.numeroassurance}
                                        change={(numeroassurance) => {
                                            this.setState({numeroassurance})
                                        }}/>
                            <InputField style={{width: "100%", height: 40}}
                                        source={require('../assets/images/icons/icons8_More_100px.png')}
                                        placeholder="Assurance complémentaire" value={this.state.complementaryassurance}
                                        change={(complementaryassurance) => {
                                            this.setState({complementaryassurance})
                                        }}/>
                        </View>}

                        <View style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignContent: "center",
                            alignItems: "center",
                            position: "relative",
                            marginTop: 25,
                            padding: 10,
                            width: "100%"
                        }}>
                            {/*put the view but with empty content in the first page of the form sections*/}
                            {this.state.position === 0 && <TouchableWithoutFeedback>
                                <Image source={require('../assets/images/icons8_Back_100px2x.png')}
                                       style={{height: 35, width: 35, opacity: 0}}/>
                            </TouchableWithoutFeedback>
                            }
                            {this.state.position > 0 && <TouchableWithoutFeedback onPress={() => {
                                if (this.state.position > 0) {
                                    this.setState({position: (this.state.position - 1)});
                                }
                            }} title="">
                                <Image source={require('../assets/images/icons8_Back_100px2x.png')}
                                       style={{height: 35, width: 35}}/>
                            </TouchableWithoutFeedback>
                            }

                            {this.state.position < 2 &&
                            <SpecialButton text="Continuer" press={() => {
                                // if(this.state.position<=2) {
                                this.setState({position: (this.state.position + 1)});
                                // }
                            }}/>
                            }

                            {this.state.position === 2 &&
                            <SpecialButton text="Completer" press={this.validateForm}/>
                            }

                        </View>

                    </View>
                </ScrollView>
                <DialogInterface visible={this.state.dialogShow} oneButton={true} onCancel={() => {
                }} negativeButton="" loading={this.state.dialogLoading} positiveButton="OK" title="Information"
                                 content={<Text style={{marginTop: 10}}>{this.state.dialogMessage}</Text>}
                                 onAccept={() => {
                                     this.setState({dialogShow: false});
                                 }}/>

            </View>
        )
    };

    componentDidMount() {
        //
    }

    validateForm() {

        let _state = this.state;
        // ToastAndroid.showWithGravity(_state,ToastAndroid.SHORT,ToastAndroid.CENTER);
        console.log(_state);
        for (let _key in _state) {
            let _value = _state[_key];
            console.log(_value);
            if (_key.includes("dialog")) {
                console.log("ignoring " + _key);
                break;
            }
            if (_value) {
                console.log("ok")
            } else {
                console.log(_key + " not ok");
                this.setState({
                    dialogLoading: false,
                    dialogShow: true,
                    dialogMessage: "Veuillez remplir tous les champs"
                });
                return;
            }
        }

        // in case everything looks good
        //
        utils.HttpPostClient(config.rootUrl + config.endpoints.signup, _state, (response) => {
            this.setState({
                dialogLoading: true});
            console.log(response);
            utils.Store(config.userDataKey, JSON.stringify(response)).then(() => {
                this.setState({
                    dialogLoading: false,
                    // dialogShow: true,
                    // dialogMessage: config.successFulSignUpMessage,
                    // onAccept: () => {
                    //     this.setState({dialogShow: false});
                    //     Actions.monCarnet();
                    // }
                });
                Actions.monCarnet();
            });
        }, () => {
            console.log("request didn't go through ");
        }, this);

    }

    styles = StyleSheet.create({
        cover: {
            backgroundColor: "white",
            height: "100%",
            display: "flex",
        },
        main_content: {
            padding: 20,
            height: "100%",
            display: "flex",
            flexDirection: "column",
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',

        },
        image: {
            marginTop: -100,
            position: "relative",
            height: 90,
            width: 90
        },
        under__scores: {
            width: "60%",
            display: "flex",
            alignContent: "center",
            justifyContent: "space-around",
            alignItems: "center",
            flexDirection: "row",
            height: 10,
            marginTop: 15,
            marginBottom: 10
        },
        under__score: {
            width: "31%",
            height: "100%",
            borderRadius: 5,
            borderColor: 'rgba(0,0,0,0.1)',
            borderWidth: 1,
        },
        back_image: {
            width: 35,
            height: 35
        }

    });

}

export default CreateAccount2;