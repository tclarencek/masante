import React, {Fragment} from 'react';
import {Text, StyleSheet, View, Image, TextInput, TouchableHighlight, ScrollView,Dimensions} from 'react-native';
import ActionBar from '../components/ActionBar';
import Utils from '../utils/utils';
import Carousel from 'react-native-snap-carousel';
import DialogInterface from '../components/DialogInterface';
import config from '../config/config';
import LinearGradient from 'react-native-linear-gradient';
let position = 0;

class conseils extends React.Component {
    constructor(props) {
        super(props);
        this.state={dialogShow:false,dialogLoading:true,dialogMessage:"..."};
        this.state.entries = [];
    }

    _render_item = ({item,index}) =>{
      return (
          <LinearGradient style={styles.imageCard}
                          colors={["#63CCE5", "#67D5B2"]}>
                <Image source={config.rootUrl+item.image}/>
          </LinearGradient>

      )
    };
    componentDidMount = ()=>{
        Utils.HttpGetClient(config.rootUrl+config.endpoints.conseils,{},(response)=>{
            this.setState({entries:response});
        },()=>{},this);
    };
    render = () => {

        return (
            <View style={styles.cover}>
                <ActionBar source={require('../assets/images/icons8_Menu_100px.png')} title="Conseils"/>
                <ScrollView style={{flexGrow: 1, width: "100%"}} contentContainerStyle={{
                    flex: 1
                }}>
                    <View style={{width: "100%", display: "flex", flexDirection: "row"}}>
                        <View style={{height: "100%", width: 10, backgroundColor: "#63CCE4"}}/>
                        <Text style={{flex: 1,padding:10,paddingLeft:20,textAlign:"justify",color:"#63CCE4",fontFamily:'Raleway',fontWeight:'900',fontSize:18}}> Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in
                            laying out print, graphic or web designs. The passage is attributed to an unknown typesetter
                            in the 15th century.</Text>

                    </View>
                    <Carousel data={this.state.entries} renderItem={this._render_item}  sliderWidth={Dimensions.get('window').width}
                              itemWidth={(Dimensions.get('window').width)}/>
                </ScrollView>
                <DialogInterface visible={this.state.dialogShow} oneButton={true} onCancel={() => {
                }} negativeButton="" loading={this.state.dialogLoading} positiveButton="OK" title="Information"
                                 content={<Text style={{marginTop: 10}}>{this.state.dialogMessage}</Text>}
                                 onAccept={() => {
                                     this.setState({dialogShow: false});
                                 }}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    cover: {
        backgroundColor: "white",
        height: "100%",
        display: "flex",
    },
    holder: {
        padding: 20,
        marginBottom: 20
    },
    imageCard:{
        height:200,
        width:200
    }

});
export default conseils;