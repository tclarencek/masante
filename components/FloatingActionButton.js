import React from 'react';
import {View,Text,TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

class FloatingActionButton extends React.Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        return (
            <TouchableOpacity {...this.props}>
                <LinearGradient colors={["rgba(99,205,227,0.2)", "rgba(104,213,182,0.2)"]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                    style={{ padding: 4, borderRadius: 50, height: 65, width: 65}}>
                    <LinearGradient colors={["#67D5B2", "#63CCE5"]} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                    style={{padding: 5, borderRadius: 50, height: "100%", width: "100%",display:"flex",justifyContent:"center",alignContent:"center",alignItems:"center"}}>
                            <Text style={{fontSize:30,fontWeight:"bold",color:"white"}}>+</Text>
                    </LinearGradient>
                </LinearGradient>
            </TouchableOpacity>
        );
    }
}

export default FloatingActionButton;