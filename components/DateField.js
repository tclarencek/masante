import React,{Component} from 'react';
import {StyleSheet,View,Image} from 'react-native';
import DatePicker from 'react-native-datepicker'

class DateField extends Component{
    constructor(props){
        super(props);
    }
    render(){

        return (
            <View style={styles.input_field} elevation={5}>
                <Image source={this.props.source} style={{marginLeft:10,position:'relative',height:25,width:25}}/>
                <DatePicker date={this.props.date} showIcon={false} mode="date" format="YYY-MM-DD" style={{width: "94.5%", height: 40, flexGrow:1,border:"none"}} placeholder={this.props.placeholder} confirmBtnText="Confirmer" cancelBtnText="Annuler"
                            onDateChange={this.props.change} customStyles={{dateInput: {
                    border: "none",
                    borderColor:"transparent",
                    textAlign:"left"
                }}}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    input_field: {
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowOffset: {
            height:2,
            width:0
        },
        elevation:5,
        width:"100%",
        height:40,
        borderColor:'rgba(0,0,0,0.1)',
        borderWidth:1,
        marginTop:15,
        shadowRadius:20,
        borderRadius:20,
        display:"flex",
        flexDirection:"row",
        alignItems:'center',
        alignContent:'center'
    }
});
export default DateField;