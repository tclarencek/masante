import React from 'react';
import {View,Image,Text,StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
class BottomBar extends React.Component{
    render(){
            return(
                <View>
                    <LinearGradient colors ={["rgba(0,0,0,0)","rgba(0,0,0,0.04)"]} style={{height:8,width:"100%"}} />
                    {this.props.view()}
                </View>
            )
    }
}
const styles = StyleSheet.create({
    action_bar:{
        display:"flex",
        flexDirection:"row",
        padding:15,
        alignItems:"center",
        alignContent:"center",
        justifyContent:"space-around"

    },
    text:{
        fontSize:20,
        marginLeft:20,
        fontFamily:'Raleway',
        color:"rgba(0,0,0,0.8)"
    },
    image:{
        height: 25,
        width:25
    }
});
export default BottomBar;