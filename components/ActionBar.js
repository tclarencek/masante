import React from 'react';
import {View,Image,Text,StyleSheet,TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
class ActionBar extends React.Component{
    render(){
            return(
                <View>
                <View style={styles.action_bar}>
                    <TouchableOpacity  onPress={this.props.onButtonPress} >
                    <Image  source={this.props.source} style={styles.image} />
                    </TouchableOpacity>
                    <Text style={styles.text}>{this.props.title}</Text>
                </View>
                <LinearGradient colors ={["rgba(0,0,0,0.04)","rgba(0,0,0,0)"]} style={{height:8,width:"100%",}} />
                </View>
            )
    }
}
const styles = StyleSheet.create({
    action_bar:{
        display:"flex",
        flexDirection:"row",
        padding:15,
        alignItems:"center",
        alignContent:"center",
        backgroundColor:"white"
    },
    text:{
        fontSize:20,
        marginLeft:20,
        fontFamily:'Raleway',
        color:"rgba(0,0,0,0.8)"
    },
    image:{
        height: 25,
        width:25
    }
});
export default ActionBar;