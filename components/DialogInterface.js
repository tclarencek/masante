import React,{Component} from 'react';
import {View,Text,StyleSheet} from 'react-native';
import Dialog,{DialogContent,DialogFooter,DialogButton,DialogTitle,ScaleAnimation} from 'react-native-popup-dialog';
// import AnimatedLoader from 'react-native-animated-loader';
import Loader from "./Loader";
export default class DialogInterface extends Component{
    static  state ={

    };
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View>
                <Loader visible={this.props.loading}/>
                <Dialog
                    dialogTitle={<DialogTitle title={this.props.title} />}
                    visible={( this.props.visible)}
                    footer={
                        <DialogFooter>

                            <DialogButton
                                text={this.props.negativeButton}
                                onPress={this.props.onCancel}
                                style={{display:this.props.oneButton?"none":"flex"}}
                            />

                            <DialogButton
                                text={this.props.positiveButton}
                                onPress={this.props.onAccept}
                            />
                        </DialogFooter>
                    }
                    dialogAnimation={new ScaleAnimation({

                    })}
                >
                    <DialogContent>
                        {this.props.content}
                    </DialogContent>
                </Dialog>
                </View>
        )
    }

}
