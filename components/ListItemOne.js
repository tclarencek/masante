import React from 'react';
import {View,Image,Text,StyleSheet,Button,TouchableOpacity,TouchableNativeFeedback} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
class ListItemOne extends React.Component{
    render(){
            return(
                <TouchableOpacity activeOpacity={0.7} >
                    <View  style={styles.main}>
                        <LinearGradient start={{x:0,y:0}} end={{x:1,y:0}} colors = {["#63CCE5","#67D5B2"]} style={styles.icon_holder}>
                            <Image source={require('../assets/images/icons/icons8_Bulleted_List_100px_1.png')} style={{height:45,width:45}}/>
                        </LinearGradient>
                        <View style={{flexGrow:1,paddingLeft:10,paddingRight:10}}>
                            <Text style={{color:"#241F1F",fontWeight:"bold",fontFamily:"Raleway",fontSize:16}}>
                                22/07/2014(Yaoundé)
                            </Text>
                            <Text style={{color:"#66D4B2",fontWeight:"bold",fontFamily:"Raleway",fontSize:16}}>
                                Paludisme
                            </Text>
                            <Text style={{color:"#241F1F",fontWeight:"bold",fontFamily:"Raleway",fontSize:10}}>
                                Hôpital cité verte
                            </Text>
                            <Text style={{color:"#241F1F",fontWeight:"bold",fontFamily:"Raleway",fontSize:12}}>
                                Dr Toko Justin.
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
    }
}
const styles = StyleSheet.create({
    main:{
        display:"flex",
        flexDirection:"row",
        alignItems:"center",
        alignContent:"center",
        borderColor:"rgba(112,112,112,0.06)",
        padding:10,
        borderWidth:1

    },
    icon_holder:{
        padding:13,
        borderRadius:20
    }
});
export default ListItemOne;