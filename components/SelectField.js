import React,{Component} from 'react';
import {StyleSheet,View,Image,Picker} from 'react-native';
import Select from 'react-native-picker-select';

class SelectField extends Component{
    render(){
        let i=0;
        return (

            <View style={styles.input_field} elevation={5}>

                <Image source={this.props.source} style={{marginLeft:10,position:'relative',height:25,width:25}}/>
               <Picker  style={{width: "93%", height: 40, flexGrow:1}} selectedValue={this.props.value} onValueChange={this.props.change}>
                   {this.props.elements.map(content => <Picker.Item label={content.label} value={content.value} key={i++}/>)}
               </Picker>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    input_field: {
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowOffset: {
            height:2,
            width:0
        },
        elevation:5,
        width:"100%",
        height:40,
        borderColor:'rgba(0,0,0,0.1)',
        borderWidth:1,
        marginTop:15,
        shadowRadius:20,
        borderRadius:20,
        display:"flex",
        flexDirection:"row",
        alignItems:'center',
        alignContent:'center'
    }
});
export default SelectField;