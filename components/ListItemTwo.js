import React from 'react';
import {View, Image, Text, StyleSheet, Button, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

class ListItemOne extends React.Component {
    render() {
        return (
            <TouchableOpacity activeOpacity={0.7}>
                <View style={styles.main}>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={["#63CCE5", "#67D5B2"]}
                                    style={styles.icon_holder}>
                        <Text style={{fontSize: 30, color: "white", textAlign:"center"}}>{this.props.number}</Text>
                    </LinearGradient>
                    <View style={{flexGrow: 1, paddingLeft: 10, paddingRight: 10,}}>
                        <Text style={{color: "#241F1F", fontFamily: "Raleway", fontSize: 20}}>
                            {this.props.nomAntecedant}
                        </Text>
                        <View style={{display: "flex",flexDirection:"row"}}>
                            <Image source={require('../assets/images/icons/icons8_Timetable_100px.png')}
                                   style={{height: 20, width: 20}}/>
                            <Text style={{
                                fontFamily: "Raleway",
                                opacity: 0.51,
                                color: "#241F1F",
                                fontWeight: "bold",
                                flexGrow: 1,
                                paddingLeft: 5
                            }}>{this.props.dateAntecedant}</Text>
                        </View>
                        <Text style={{color: "#241F1F", fontWeight: "bold", fontFamily: "Raleway"}}>
                            {this.props.lieuAntecedant}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        alignContent: "center",
        borderColor: "rgba(112,112,112,0.06)",
        padding: 10,
        borderWidth: 1

    },
    icon_holder: {
        height: 75,
        width: 75,
        borderRadius: 300,
        flexDirection: "row",
        alignItems: "center",
        alignContent: "center",
        justifyContent: "center"
    }
});
export default ListItemOne;