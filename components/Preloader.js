import React,{Component} from 'react';
import config from '../config/config';
import Loader from './Loader';
export default class Preloader extends Component{
    constructor(props){
        super(props);
        this.state={show:true};
    }
    render(){
        return(
             <Loader visible={this.state.show} />
        )
    }
    componentDidMount(){
        setTimeout(()=>{
            this.setState({show:false});
        },config.preloaderTimeOut)
    }
}