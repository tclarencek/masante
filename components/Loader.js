import React,{Component} from 'react';
import {StyleSheet} from 'react-native';
import AnimatedLoader from 'react-native-animated-loader';
export default class Loader extends Component{
    render(){
        return(
            <AnimatedLoader
                visible={this.props.visible}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("./../assets/loader/circle-load2.json")}
                animationStyle={styles.lottie}
                speed={1}
            />
        )
    }
}
const styles = StyleSheet.create({
    lottie: {
        width: 100,
        height: 100
    }
});