import config from '../config/config';
import {AsyncStorage, ToastAndroid} from 'react-native';
import DateCalculator from 'date-calculator';
import ImagePicker from 'react-native-image-picker';

class Utils {
    //https://github.com/react-native-community/react-native-image-picker
    /***
     *
     * @param _title String
     * @param _context Object
     */
    static chooseFile(_title,_context){
        let pickerOptions ={
          title:_title,
            storageOptions:{
              path:'ma_sante_images'
            }
        };

        ImagePicker.showImagePicker(pickerOptions,(response)=>{
            console.log(response);
            if(response.didCancel){
                console.log("user cancelled filechoose");
            }else if(response.error){
                _context.setState({dialogLoading: false});
                _context.setState({dialogMessage: config.fileChooseFailed});
                _context.setState({dialogShow: true});
            }else{
                const source = { uri: response.uri };
                console.log("filechoose received file");
                console.log(source);
                _context.setState({fileChoosen:source});
            }
        });

    }

    /**
     *
     * @param _date eg 2019-12-25
     * @returns {{days, weeks, months, years}}
     */
    static calculateDateDiff(_date){
        let split_date =  _date.split("-");
        if(split_date.length !== 3){
            console.log("date is not in format YYYY-MM-DD");
        }
        let dc = new DateCalculator(new Date());
        let resp =  dc.sub(new Date(split_date[0],split_date[1],split_date[2]));
        console.log("date difference: ");
        console.log(resp);
        return resp;
    }

    static async Store(_key, _value) {
        try {
            console.log("trying to store "+_key+" and value: "+_value);
           await AsyncStorage.setItem(_key, _value);
           console.log("done storing "+_key);
        } catch (exception) {
            ToastAndroid.showWithGravity(config.asyncStorageReadError, ToastAndroid.LONG, ToastAndroid.BOTTOM);
        }
    }

    static async Load(_key, _default) {
        let _return = _default;
        try {
            let _value = AsyncStorage.getItem(_key);
            _return = _value ? _value : _default;
        }
        catch (error) {
            ToastAndroid.showWithGravity(config.asyncStorageReadError, ToastAndroid.LONG, ToastAndroid.BOTTOM);
        }
        return _return;
    }

    static async storageHas(_key) {
       return  Utils.Load(_key, null).then((_value) => {
            let _result =  _value? true: false;
            console.log("Storage has "+_key+"?: " + _result);
           return _result;
        })
    }

    static async  userLoggedIn(){
        return Utils.storageHas(config.userDataKey).then((response)=>{console.log("user logged in?: "+response);return response});
    }
    static getUserData(){
        return Utils.Load(config.userDataKey,null).then((response)=>{console.log(response);return response});
    }
    static HttpPostClient = (url, data, successCallBack, errorCallback, object) => {
        Utils.HttpClient(url, "POST", data, successCallBack, errorCallback, object);
    };
    static HttpGetClient = (url, data, successCallBack, errorCallback, object) => {
        Utils.HttpClient(url, "POST", data, successCallBack, errorCallback, object);
    };

    /**
     *
     * @param url
     * @param method :"POST" or "GET"
     * @param data : JSON data in Json format (JS object)
     * @param successCallBack : function doesn't necessarily mean the response is positvie. just that it
     * has reached the se
     * rver and back
     * @param errorCallback : in case of an exception caught...
     * @param object : which is the current class with state variables as    dialogLoading:false, dialogShow:false, dialogMessage:"Message here"
     * @constructor
     */
    static HttpClient = (url, method, data, successCallBack, errorCallback, object) => {
        object.setState({dialogLoading: true});
        fetch(url, {
            method,
            body: JSON.stringify(data)
        }).then((r) => r.json()).then((response)=>{
            console.log("received data from "+url);
            console.log(response);
            if (response.error) {
                object.setState({dialogLoading: false});
                object.setState({dialogMessage: response.error});
                object.setState({dialogShow: true});
                //    print the server generated error message
            } else {
                object.setState({dialogLoading: false});
                successCallBack(response);
            }
        }).catch((error) => {
            //generated a default error message from the config file
            let message = config.httpErrorMessage;
            object.setState({dialogLoading: false});
            object.setState({dialogMessage: message});
            object.setState({dialogShow: true});
            errorCallback();
            console.log("failed to reach server at: "+url);
            console.log(error);
        })
    }
}

export default Utils;