export default ConfigData = {
    name: "ma_sante",
    displayName: "ma_sante",
    rootUrl: "http://192.168.43.116:1998",
    //inscripton page
    genders: [{
        "label":"Male",
        "value":"M"
    },{
        "label":"Female",
        "value":"F"
    }],
    bloodgroups:[{
        "label":"A",
        "value":"A"
    },{
        "label":"B",
        "value":"B"
    },{
        "label":"C",
        "value":"C"
    },{
        "label":"O",
        "value":"O"
    }],
    rhesus:[{
        "label":"Plus",
        "value":"+",
    },{
        "label":"Moins",
        "value":"-"
    }],
    //http client end points
    endpoints:{
        login:"/api/login",
        signup:"/api/signup",
        conseils:"/api/conseils.json",
        antecedentCreate:"/api/antecedant/new",
        antecedents:"/api/antecedents.json"
    },
    asyncStorageWriteError:"Une erreur d'ecriture s'est produit",
    asyncStorageReadError:"Une erreur de lecture s'est produit",
    successFulSignUpMessage:"Inscription avec suces",
//  when the Http client has caught an exception eg 404,403,500 etc. in the utils file
    httpErrorMessage:"Erreur de connexion au server, Veuillez reesayer",
//  page preloader time in ms
    preloaderTimeOut:5000,
//    user data is stored with this key
    userDataKey:"u-data",

    fileChooseFailed:"Erreur: Veuillez re-selectionner un fichier",

    typeDAntecedants:[{
        'value':'AA',
        'label':'Antécédent Allergiques'
    },{
        'value':'AC',
        'label':'Antécédents Chirurgicaux'
    },{
        'value':'AF',
        'label':'Antécédent Familiaux'
    },{
        'value':'AM',
        'label':'Antécédents Médicaux'
    }],

    nothing_to_show:"Rien a montrer"

};

/*********************************/
/*

Contains specific params for certain requests

1. Antecedents list : ==> /api/antecedants?api_token=asadklXXUsajhsa

 */